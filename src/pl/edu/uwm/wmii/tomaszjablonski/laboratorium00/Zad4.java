package pl.edu.uwm.wmii.tomaszjablonski.laboratorium00;

public class Zad4 {
    static void saldo(double money, double perc, int year) {

        for(int i = 0; i < year ; i++) {
            money = money - (money / 100) * perc ;
            System.out.println((i+1) + " rok: " + money);
        }
    }
    public static void main(String[] args) {
        saldo(1000, 6, 3);
    }

}
