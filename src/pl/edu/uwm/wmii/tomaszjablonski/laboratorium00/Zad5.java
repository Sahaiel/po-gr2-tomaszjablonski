package pl.edu.uwm.wmii.tomaszjablonski.laboratorium00;

public class Zad5 {
    static void ramka(String napis) {
        int length = napis.length();
        System.out.print('+');
        for(int i = 0; i < length; i++) {
            System.out.print('-');
        }
        System.out.println('+');
        System.out.println("|" + napis + "|");
        System.out.print('+');
        for(int i = 0; i < length; i++) {
            System.out.print('-');
        }
        System.out.print('+');

    }
    public static void main(String[] args) {
        String napis = "Java";
        ramka(napis);

    }
}
