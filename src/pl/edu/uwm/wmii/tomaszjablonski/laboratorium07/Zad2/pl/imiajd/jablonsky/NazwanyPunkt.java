package pl.edu.uwm.wmii.tomaszjablonski.laboratorium07.Zad2.pl.imiajd.jablonsky;


public class NazwanyPunkt extends Punkt
{
    public NazwanyPunkt(int x, int y, String name)
    {
        super(x, y);
        this.name = name;
    }

    public void show()
    {
        System.out.println(name + ":<" + x() + ", " + y() + ">");
    }

    private String name;
}
