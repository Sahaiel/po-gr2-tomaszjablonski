package pl.edu.uwm.wmii.tomaszjablonski.laboratorium07.Zad2.pl.imiajd.jablonsky;

import java.util.*;

public class Adres {
    public Adres(int numer_mieszkania) {
        this.numer_mieszkania = numer_mieszkania;
    }

    public Adres() {

    }

    public void pokaz() {
        System.out.println( this.kod_pocztowy + " " + this.miasto);
        System.out.println(this.ulica + " " + this.numer_domu + " " + this.numer_mieszkania);
    }

    public boolean przed(String kod_pocztowy) {
        if(Integer.parseInt(kod_pocztowy.substring(0,2)) < Integer.parseInt(this.kod_pocztowy.substring(0,2))) {
            return true;
        }
        else if(Integer.parseInt(kod_pocztowy.substring(3)) < Integer.parseInt(this.kod_pocztowy.substring(3))) {
            return true;
        }
        else return false;

    }

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;

}
