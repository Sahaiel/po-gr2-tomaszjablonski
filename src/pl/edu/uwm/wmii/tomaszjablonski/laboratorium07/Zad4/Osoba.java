package pl.edu.uwm.wmii.tomaszjablonski.laboratorium07.Zad4;


import java.util.*;

public class Osoba {
    private String nazwisko;
    private int rok_urodzenia;

    public Osoba(String nazwisko, int rok_urodzenia) {
        this.nazwisko = nazwisko;
        this.rok_urodzenia = rok_urodzenia;
    }

    public String toString() {
        String tmp = "";
        tmp += this.nazwisko + " " + rok_urodzenia;
        return tmp;
    }
}
