package pl.edu.uwm.wmii.tomaszjablonski.laboratorium07.Zad4;

public class Nauczyciel extends Osoba {
    private double pensja;

    public Nauczyciel(double pensja, String nazwisko, int rok_urodzenia) {
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }

    public String toString() {
        String tmp = "";
        tmp += this.pensja;
        return tmp;
    }
}
