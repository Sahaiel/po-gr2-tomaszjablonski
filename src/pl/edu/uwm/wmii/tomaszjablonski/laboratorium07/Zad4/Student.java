package pl.edu.uwm.wmii.tomaszjablonski.laboratorium07.Zad4;

public class Student extends Osoba {
    private String kierunek;

    public Student(String kierunek, String nazwisko, int rok_urodzenia) {
        super(nazwisko, rok_urodzenia);
        this.kierunek = kierunek;

    }

    public String getKierunek() {

        return this.kierunek;
    }
}
