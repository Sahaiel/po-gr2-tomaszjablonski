package pl.edu.uwm.wmii.tomaszjablonski.laboratorium03;

import java.util.Scanner;

public class Zad1c {
    public static String middle(String str) {
        if(str.length() % 2 == 0) {
            return str.substring((str.length() / 2) - 1, (str.length() / 2) + 1);
        }
        else {
            return str.substring((str.length() / 2), (str.length() / 2) + 1);
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str, subStr;
        System.out.println("Wprowadz napis: ");
        str = in.nextLine();

        System.out.println("Wynik: " + middle(str));
    }
}