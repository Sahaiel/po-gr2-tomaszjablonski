package pl.edu.uwm.wmii.tomaszjablonski.laboratorium03;

import java.util.Scanner;

public class Zad1e {
    public static int[] where(String str, String subStr) {
        int counter = 0;
        for(int i = 0; i <= str.length() - subStr.length(); i++) {
            if(subStr.equals(str.substring(i,i+subStr.length()))) {
                counter++;
            }
        }

        int tab[] = new int[counter];
        for(int i = 0, temp = 0; i <= str.length() - subStr.length(); i++) {
            if(subStr.equals(str.substring(i,i+subStr.length()))) {
                tab[temp] = i;
                temp++;
            }
        }
        return tab;

    }
    public static void wypisz(int tab[]) {
        for(int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str, subStr;
        System.out.println("Wprowadz napis: ");
        str = in.nextLine();


        System.out.println("Wprowadz ciąg znakowy: ");
        subStr = in.nextLine();

        wypisz(where(str, subStr));

    }
}