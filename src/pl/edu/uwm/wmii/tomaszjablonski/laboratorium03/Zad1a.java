package pl.edu.uwm.wmii.tomaszjablonski.laboratorium03;

import java.util.Scanner;

public class Zad1a {
    public static int countChar(String str, char c) {
        int counter = 0;
        for( int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == c) counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str;
        System.out.println("Wprowadz napis: ");
        str = in.nextLine();

        char c;
        System.out.println("Wprowadz znak: ");
        c = in.next().charAt(0);

        System.out.println("Wynik: " + countChar(str, c));
    }
}
