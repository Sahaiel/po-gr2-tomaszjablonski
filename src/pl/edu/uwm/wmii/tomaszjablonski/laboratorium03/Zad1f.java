package pl.edu.uwm.wmii.tomaszjablonski.laboratorium03;

import java.util.Scanner;

public class Zad1f {
    public static String change(String str) {
        StringBuffer strbuff = new StringBuffer();
        for(int i = 0; i < str.length(); i++) {
            int tmp = (int)str.charAt(i);
                if(tmp >= 65 && tmp <=90 ) tmp += 32;
                else if(tmp >= 97 && tmp <=122) tmp -= 32;
                strbuff.append((char)tmp);
        }
        return strbuff.toString();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str, subStr;
        System.out.println("Wprowadz napis: ");
        str = in.nextLine();
        //System.out.println("A:" + (int)'A' +" Z:" + (int)'Z' + " a:" + (int)'a'+ " z:" + (int)'z');
        System.out.println(change(str));
    }
}