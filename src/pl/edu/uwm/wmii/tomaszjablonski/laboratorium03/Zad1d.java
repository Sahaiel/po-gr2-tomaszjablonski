package pl.edu.uwm.wmii.tomaszjablonski.laboratorium03;

import java.util.Scanner;

public class Zad1d {
    public static String repeat(String str, int n) {
        String temp = "";
        for(int i = 0; i < n; i++) {
            temp += str;
        }
        return temp;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str, subStr;
        System.out.println("Wprowadz napis: ");
        str = in.nextLine();

        int n = 0;
        System.out.println("Wprowadz n: ");
        n = in.nextInt();

        System.out.println("Wynik: " + repeat(str, n));
    }
}