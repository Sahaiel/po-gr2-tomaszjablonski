package pl.edu.uwm.wmii.tomaszjablonski.laboratorium03;

import java.util.Scanner;

public class Zad1b {
    public static int countSubStr(String str, String subStr) {
        int counter = 0;
        for(int i = 0; i <= str.length() - subStr.length(); i++) {
            if(subStr.equals(str.substring(i,i+subStr.length()))) counter++;
        }
        return counter;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str, subStr;
        System.out.println("Wprowadz napis: ");
        str = in.nextLine();


        System.out.println("Wprowadz ciąg znakowy: ");
        subStr = in.nextLine();

        System.out.println("Wynik: " + countSubStr(str, subStr));


    }
}
