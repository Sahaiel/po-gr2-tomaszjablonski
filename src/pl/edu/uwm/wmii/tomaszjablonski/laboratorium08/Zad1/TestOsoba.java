package pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad1;
import java.util.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        String[] array = {"Tomasz", "Adam"};
        ludzie[0] = new Pracownik("Jabłoński", 50000, array, "1997-10-01", "2010-11-08" );
        ludzie[1] = new Student("Małgorzata Nowak", "informatyka", array, "1997-10-01", 4.5);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }

      System.out.println(ludzie[0].toString());
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, String dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = LocalDate.parse(dataUrodzenia, formatter);
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona() {
        String temp = "";
        for( String el : imiona) {
            temp += " " + el;
        }
        return temp;
    }
    public String getDataUrodzenia() {
        return dataUrodzenia.format(formatter);
    }
    public String toString() {
        String temp = "";
        temp += nazwisko + getImiona() + " " + getDataUrodzenia();
        return temp;
    }

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private String nazwisko;
    private String[] imiona;
    LocalDate dataUrodzenia;
}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,double pobory, String[] imiona, String dataUrodzenia, String dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia = LocalDate.parse(dataZatrudnienia, formatter);
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    public String getDataZatrudnienia() { return dataZatrudnienia.format(formatter); }


    private double pobory;
    private LocalDate dataZatrudnienia;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
}


class Student extends Osoba
{
    public Student(String nazwisko,String kierunek, String[] imiona, String dataUrodzenia, double sredniaOcen )
    {
        super(nazwisko, imiona, dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public void setSredniaOcen(double srednia) {
        this.sredniaOcen = srednia;
    }
    public double getSredniaOcen() {
        return sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}

