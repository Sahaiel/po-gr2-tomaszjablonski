package pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2.pl.imiajd.jablonsky;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,double pobory, String[] imiona, String dataUrodzenia, String dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia = LocalDate.parse(dataZatrudnienia, formatter);
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    public String getDataZatrudnienia() { return dataZatrudnienia.format(formatter); }


    private double pobory;
    private LocalDate dataZatrudnienia;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
}
