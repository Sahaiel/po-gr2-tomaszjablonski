package pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2;
import pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2.pl.imiajd.jablonsky.Osoba;
import pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2.pl.imiajd.jablonsky.Pracownik;
import pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2.pl.imiajd.jablonsky.Student;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        String[] array = {"Tomasz", "Adam"};
        ludzie[0] = new Pracownik("Jabłoński", 50000, array, "1997-10-01", "2010-11-08" );
        ludzie[1] = new Student("Małgorzata Nowak", "informatyka", array, "1997-10-01", 4.5);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }

      System.out.println(ludzie[0].toString());
    }
}


