package pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2.pl.imiajd.jablonsky;

public class Student extends Osoba
{
    public Student(String nazwisko,String kierunek, String[] imiona, String dataUrodzenia, double sredniaOcen )
    {
        super(nazwisko, imiona, dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public void setSredniaOcen(double srednia) {
        this.sredniaOcen = srednia;
    }
    public double getSredniaOcen() {
        return sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}
