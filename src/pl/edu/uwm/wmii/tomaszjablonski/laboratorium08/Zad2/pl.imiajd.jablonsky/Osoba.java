package pl.edu.uwm.wmii.tomaszjablonski.laboratorium08.Zad2.pl.imiajd.jablonsky;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, String dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = LocalDate.parse(dataUrodzenia, formatter);
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona() {
        String temp = "";
        for( String el : imiona) {
            temp += " " + el;
        }
        return temp;
    }
    public String getDataUrodzenia() {
        return dataUrodzenia.format(formatter);
    }
    public String toString() {
        String temp = "";
        temp += nazwisko + getImiona() + " " + getDataUrodzenia();
        return temp;
    }

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private String nazwisko;
    private String[] imiona;
    LocalDate dataUrodzenia;
}
