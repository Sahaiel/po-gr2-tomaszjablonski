package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad1f {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random(System.currentTimeMillis());
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }


        int tab[] = new int[n];
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
        }

        for(int i = 0; i < n; i++) {
            if(tab[i] > 0) tab[i] = 1;
            else if(tab[i] < 0) tab[i] = -1;
        }

        for(int i = 0; i < n; i++) {
            System.out.print(tab[i] + " ");
            //if(i % 5 == 0) System.out.print('\n');
        }


    }
}
