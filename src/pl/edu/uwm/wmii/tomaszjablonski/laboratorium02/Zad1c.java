package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad1c {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random(System.currentTimeMillis());
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }

        int max = -999, counter = 0;

        int tab[] = new int[n];
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if( tab[i] > max ) {
                max = tab[i];
                counter = 1;
            }
            else if (tab[i] == max) counter++;

        }


        System.out.println("Liczba najwieksza: " + max);
        System.out.println("Liczba wystapien: " + counter);



    }
}
