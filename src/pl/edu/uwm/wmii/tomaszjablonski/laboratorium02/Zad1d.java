package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad1d {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random(System.currentTimeMillis());
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }

        int suminc = 0, sumdec = 0;

        int tab[] = new int[n];
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if( tab[i] > 0 ) suminc += tab[i];
            else sumdec += tab[i];



        }


        System.out.println("Suma liczb dodatnich: " + suminc);
        System.out.println("Suma liczb ujemnych: " + sumdec);



    }
}
