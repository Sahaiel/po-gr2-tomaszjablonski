package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad1b {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random(System.currentTimeMillis());
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }

        int inc = 0, dec = 0, zeros = 0;

        int tab[] = new int[n];
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if( tab[i] > 0 ) inc++;
            else if( tab[i] < 0) dec++;
            else zeros++;
        }


        System.out.println("Liczba dodatnich liczb: " + inc);
        System.out.println("Liczba ujemnych liczb: " + dec);
        System.out.println("Liczba zer: " + zeros);


    }
}
