package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad2g {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random rand = new Random(System.currentTimeMillis());
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }
    public static void wypisz(int tab[]) {
        for (int i = 0; i < tab.length; i++)
        {
            System.out.print(tab[i] + " ");
            if(i % 10 == 0 && i > 0)  System.out.println("");
        }
        System.out.println("\n__________________________________________________________");
    }
    public static void odwrocFragment(int tab[], int lewy, int prawy) {
        for(int temp = 0; lewy < prawy; lewy++, prawy--) {
            temp = tab[lewy];
            tab[lewy] = tab[prawy];
            tab[prawy] = temp;
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }
        int lewy = 0, prawy = 0;
        while(lewy < 1 || lewy >= n) {
            System.out.println("Podaj lewy przedzial: ");
            lewy = in.nextInt();
        }
        while(prawy < 1 || prawy >= n) {
            System.out.println("Podaj prawy przedzial: ");
            prawy = in.nextInt();
        }

        int tab[] = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab);
        odwrocFragment(tab, lewy, prawy);
        wypisz(tab);

    }
}

