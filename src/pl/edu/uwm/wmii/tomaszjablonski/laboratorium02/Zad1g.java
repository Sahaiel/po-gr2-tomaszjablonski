package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad1g {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random(System.currentTimeMillis());
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }

        int tab[] = new int[n];
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
        }

        int lewy = 0, prawy = 0;
        while(lewy < 1 || lewy >= n) {
            System.out.println("Podaj lewy przedzial: ");
            lewy = in.nextInt();
        }
        while(prawy < 1 || prawy >= n) {
            System.out.println("Podaj prawy przedzial: ");
            prawy = in.nextInt();
        }

        for(int i = 0; i < n; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.print('\n');

        for(int temp = 0; lewy < prawy; lewy++, prawy--) {
           temp = tab[lewy];
           tab[lewy] = tab[prawy];
           tab[prawy] = temp;
        }

        for(int i = 0; i < n; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.print('\n');
    }



}





