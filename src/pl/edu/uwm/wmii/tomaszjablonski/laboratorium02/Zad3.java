package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad3 {
    public static void generuj(int tab[], int minWartosc, int maxWartosc) {
        Random rand = new Random();
        for(int i = 0; i < tab.length; i++) {
            tab[i] = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }
    public static void generujMacierz(int tab[][], int minWartosc, int maxWartosc) {
        for(int i = 0; i < tab.length; i++) {
            generuj(tab[i], minWartosc, maxWartosc);
        }
    }
    public static void wypiszMacierz(int tab[][]) {
        for(int i = 0; i < tab.length; i++) {
            for(int el : tab[i]) {
                System.out.print(el + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    public static void mnozMacierz(int tab[][], int tab2[][], int wynik[][]) {
        for(int i = 0;i < wynik.length; i++) {
            for(int j = 0; j < wynik[i].length; j++) {
                for(int h = 0; h < tab2.length; h++) {
                    wynik[i][j] += tab[i][h] * tab2[h][j];
                }
            }
        }

    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 0, m = 0, k = 0;
        while(n < 1 || n > 10) {
            System.out.println("Wprowadz n: ");
            n = in.nextInt();
        }
        while(m < 1 || m > 10) {
            System.out.println("Wprowadz m: ");
            m = in.nextInt();
        }
        while(k < 1 || k > 10) {
            System.out.println("Wprowadz k: ");
            k = in.nextInt();
        }

        int a[][] = new int[m][n];
        int b[][] = new int[n][k];
        int c[][] = new int[m][k];
        generujMacierz(a, 0, 9);
        generujMacierz(b, 0, 9);

        wypiszMacierz(a);
        wypiszMacierz(b);
        mnozMacierz(a, b, c);
        wypiszMacierz(c);
        //test

    }
}
