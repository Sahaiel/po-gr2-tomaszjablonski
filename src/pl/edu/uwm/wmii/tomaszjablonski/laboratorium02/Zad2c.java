package pl.edu.uwm.wmii.tomaszjablonski.laboratorium02;
import java.util.Scanner;

import java.util.Random;

public class Zad2c {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random rand = new Random(System.currentTimeMillis());
        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }
    public static void wypisz(int tab[]) {
        for (int i = 0; i < tab.length; i++)
        {
            System.out.print(tab[i] + " ");
            if(i % 10 == 0 && i > 0)  System.out.println("");
        }
        System.out.println("\n__________________________________________________________");
    }
    public static int ileMaksymalnych(int tab[]) {
        int max = 0, wynik = 0;
        for (int i = 0; i < tab.length; i++)
        {
            if( tab[i] > max ) {
                max = tab[i];
                wynik = 1;
            }
            else if (tab[i] == max) wynik++;
        }
        return wynik;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = -1;
        while(n < 1 || n > 1000) {
            System.out.println("Wprowadz liczbe od 1 do 1000: ");
            n = in.nextInt();
        }

        int tab[] = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab);
        System.out.println(ileMaksymalnych(tab));

    }
}
