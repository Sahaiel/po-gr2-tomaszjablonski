package pl.edu.uwm.wmii.tomaszjablonski.laboratorium06.Zad2;

import java.util.*;

public class IntegerSet {
    public IntegerSet() {
        this.tab = new boolean[100];
    }

    static boolean[] union(boolean tab1[], boolean tab2[]) {
        boolean[] temp = new boolean[tab1.length > tab2.length ? tab1.length : tab2.length];
        for(int i = 0; i < temp.length; i++) {
            if( tab1[i] || tab2[i]) temp[i] = true;
        }
        return temp;
    }

    static boolean[] intersection(boolean tab1[], boolean tab2[]) {
        boolean[] temp = new boolean[tab1.length > tab2.length ? tab1.length : tab2.length];
        for(int i = 0; i < temp.length; i++) {
            if( tab1[i] && tab2[i]) temp[i] = true;
        }
        return temp;
    }

    void insertElement(int value) {
        this.tab[value-1] = true;
    }

    void deleteElement(int value) {
        this.tab[value-1] = false;
    }

    public String toString() {
        String temp = "";
        for(int i = 0; i < this.tab.length; i++) {
            if( this.tab[i] )
                temp+=(i+1) + " ";
        }

        return temp;
    }

    boolean equals(boolean[] tab) {
        if(this.tab.length != tab.length ) return false;
        for(int i = 0; i < tab.length; i++) {
            if(this.tab[i] != tab[i]) return false;
        }
        return true;
    }
    private boolean[]tab;
}


