package pl.edu.uwm.wmii.tomaszjablonski.laboratorium06.Zad1;

import java.util.Scanner;

public class Zad1 {
    public static void main(String[] args) {
        RachunekBankowy.setRocznaStopaProcentowa(350);
        System.out.println(RachunekBankowy.rocznaStopaProcentowa);

        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(4);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saver1 saldo: " + saver1.getSaldo());
        System.out.println("saver2 saldo: " + saver2.getSaldo());

    }
}



