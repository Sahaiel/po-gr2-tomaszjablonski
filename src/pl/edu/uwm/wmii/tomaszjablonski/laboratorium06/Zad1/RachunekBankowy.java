package pl.edu.uwm.wmii.tomaszjablonski.laboratorium06.Zad1;

class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo = 0;

    public RachunekBankowy(double value) {
        this.saldo = value;
    }

    void obliczMiesieczneOdsetki() {
        double temp = this.saldo * this.rocznaStopaProcentowa / 12;
        this.saldo += temp;
        System.out.println(temp);
    }

    static void setRocznaStopaProcentowa(double value) {
        rocznaStopaProcentowa = value / 100;
    }

    public double getSaldo() {
        return this.saldo;
    }
}
