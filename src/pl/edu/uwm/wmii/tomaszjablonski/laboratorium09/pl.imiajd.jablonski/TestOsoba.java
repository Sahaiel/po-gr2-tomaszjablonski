package pl.edu.uwm.wmii.tomaszjablonski.laboratorium09.pl.imiajd.jablonski;

import pl.edu.uwm.wmii.tomaszjablonski.laboratorium09.pl.imiajd.jablonski.Osoba;

import java.util.Arrays;

public  class TestOsoba {
    public static void main(String[] args) {
        Osoba grupa[] = new Osoba[5];

        grupa[0] = new Osoba("Jab", "1997-01-07");
        grupa[1] = new Osoba("Jab", "1997-01-06");
        grupa[2] = new Osoba("Beg", "1997-02-01");
        grupa[3] = new Osoba("Geg", "1997-03-01");
        grupa[4] = new Osoba("Ded", "1997-03-01");

        for(int i = 0; i < 5; i++) {
            System.out.println(grupa[i].toString());
        }

        Arrays.sort(grupa);

        System.out.println("////After sort/////");

        for(int i = 0; i < 5; i++) {
            System.out.println(grupa[i].toString());
        }

        System.out.println("////Lata:Miesiace:Dni/////");
        for(int i = 0; i < 5; i++) {
            System.out.println("Lat:"+grupa[i].ileLat()+" Miesiecy:"+grupa[i].ileMiesiecy()+" Dni:"+grupa[i].ileDni());
        }

    }
}
