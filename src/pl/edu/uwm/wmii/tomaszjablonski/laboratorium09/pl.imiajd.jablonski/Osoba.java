package pl.edu.uwm.wmii.tomaszjablonski.laboratorium09.pl.imiajd.jablonski;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Osoba implements Comparable<Osoba> {
    public Osoba(String nazwisko, String dataUrodzenia) {
        this.dataUrodzenia = LocalDate.parse(dataUrodzenia, formatter);
        this.nazwisko = nazwisko;
    }

    public int ileLat() {
        return Period.between(this.dataUrodzenia, LocalDate.now()).getYears();
    }

    public int ileMiesiecy() {
        return Period.between(this.dataUrodzenia, LocalDate.now()).getMonths();
    }

    public int ileDni() {
        return Period.between(this.dataUrodzenia, LocalDate.now()).getDays();
    }

    @Override
    public String toString() {
        return "Osoba[" +
                "nazwisko='" + nazwisko + '\'' +
                ", dataUrodzenia=" + dataUrodzenia.format(formatter) +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwisko, dataUrodzenia);
    }

    @Override
    public int compareTo(Osoba o) {
        if(this.nazwisko.compareTo(o.nazwisko) == 0) {
            if (this.dataUrodzenia.isAfter(o.dataUrodzenia))
                return 1;
            else if (this.dataUrodzenia.isBefore(o.dataUrodzenia))
                return -1;
            else
                return 0;
        }
        return this.nazwisko.compareTo(o.nazwisko);
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
}
